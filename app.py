from flask import request
from flask import Flask
from utils import check_id

app = Flask(__name__)


@app.route("/", methods=["GET"])
def index():
    g_id = request.args.get("id")
    g_zid = request.args.get("zid")
    with open("data/vars.txt", "w") as f:
        f.write(f"{g_id} {g_zid}")
    with open("data/out.txt", "w") as f:
        f.write("1")

    while 1:
        with open("data/out.txt", "r") as f:
            out_resp = f.read()

        if out_resp == "1":
            with open("data/vars.txt", "r") as f:
                vars_data = f.read().split(" ")

            name_resp = check_id(vars_data[0], vars_data[1])

            with open("data/ret.txt", "w", encoding="utf8") as f:
                f.write(str(name_resp))

            with open("data/out.txt", "w") as f:
                f.write("0")
        else:
            break

    return name_resp


if __name__ == "__main__":
    app.run(debug=True)
