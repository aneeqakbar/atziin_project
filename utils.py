from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
import time

def get_browser():
    capa = DesiredCapabilities.CHROME
    capa["pageLoadStrategy"] = "none"
    browser = None
    chrome_options = Options()

    chrome_options.add_argument("--headless")
    chrome_options.add_argument('--profile-directory=Default')
    chrome_options.add_argument('--user-data-dir=C:/Temp/ChromeProfile')

    if not browser:
                browser = webdriver.Chrome(
                    ChromeDriverManager().install(),
                    options=chrome_options,
                    desired_capabilities=capa
                )
    browser = browser
    browser.maximize_window()
    browser.get('https://www.smile.one/merchant/mobilelegends?source=googleads')
    time.sleep(5)
    return browser

def check_id(u_id, z_id):
    browser = get_browser()
    wait = WebDriverWait(browser, 600)
    browser.refresh()
    time.sleep(1)
    user_id_xpath = '//*[@id="user_id"]'
    zone_id_xpath = '//*[@id="zone_id"]'
    user_id_input_box = wait.until(EC.presence_of_element_located((By.XPATH, user_id_xpath)))
    zone_id_input_box = wait.until(EC.presence_of_element_located((By.XPATH, zone_id_xpath)))

    user_id_input_box.send_keys(Keys.CONTROL + "a")
    user_id_input_box.send_keys(Keys.DELETE)
    user_id_input_box.send_keys(u_id)
    zone_id_input_box.send_keys(Keys.CONTROL + "a")
    zone_id_input_box.send_keys(Keys.DELETE)
    zone_id_input_box.send_keys(z_id)

    time.sleep(1)
    empty_xpath = '/html/body/div[1]/div[1]/div/div[1]/div[2]/div[2]/div/div[1]/h2'
    browser.find_element("xpath", empty_xpath).click()


    time.sleep(2)
    name_xpath = '//*[@id="mnickname"]'
    name = browser.find_element("xpath", name_xpath).text
    if name != '':
        name = f'{name}'
    else:
        name = 'Name not available!'

    browser.switch_to.window(browser.window_handles[0])
    time.sleep(0.5)

    return name